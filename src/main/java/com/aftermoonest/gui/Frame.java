package com.aftermoonest.gui;

import com.aftermoonest.search.Search;
import mdlaf.animation.MaterialUIMovement;
import mdlaf.utils.MaterialColors;

import javax.swing.*;
import java.awt.event.*;
import java.io.*;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Timer;
import java.util.TimerTask;

public class Frame extends MainFrame {

    //region initialization frame
    private JPanel panel;
    private JTextField lengthTextField;
    private JTextField minTextField;
    private JTextField maxTextField;
    private JButton byHandButton;
    private JTabbedPane tabbedPane;
    private JButton randomButton;
    private JButton exitButton;
    private JTable table;
    private JButton clearButton;
    private JLabel arrayLabel;
    private JLabel enterLengthLabel;
    private JPanel enterByHandPanel;
    private JButton submitButton;
    private JScrollPane arrayTab;
    private JScrollPane stepsTab;
    private JScrollPane enterByHandTab;

    private JLabel stepsLabel;

    public JLabel getStepsLabel() {
        return stepsLabel;
    }

    private JLabel clearedLabel;
    private JButton individualButton;
    private JLabel timeOfAlgorithmLabel;
    private JLabel countOfComparationLabel;
    private JTextField searchElementTextField;
    private JLabel indexOfSearchingElementLabel;
    //endregion

    //region data for array
    private int length;
    private int min;
    private int max;
    //endregion

    //region constants
    private final int DEFAULT_LENGTH = 10;
    private final int DEFAULT_MIN = -10;
    private final int DEFAULT_MAX = 10;

    private final int DEFAULT_TABBED_PANE_X = 500;
    private final int DEFAULT_TABBED_PANE_Y = 300;
    //endregion

    Search search;

    @Override
    void createView() {
        frame.setTitle("Shell Sort");

        //region UI
        byHandButton.setBackground(MaterialColors.ORANGE_800);
        byHandButton.setForeground(MaterialColors.WHITE);
        byHandButton.addMouseListener(MaterialUIMovement.getMovement(byHandButton, MaterialColors.ORANGE_500));

        randomButton.setBackground(MaterialColors.ORANGE_800);
        randomButton.setForeground(MaterialColors.WHITE);
        randomButton.addMouseListener(MaterialUIMovement.getMovement(randomButton, MaterialColors.ORANGE_500));

        exitButton.setBackground(MaterialColors.RED_800);
        exitButton.setForeground(MaterialColors.WHITE);
        exitButton.addMouseListener(MaterialUIMovement.getMovement(exitButton, MaterialColors.RED_500));

        clearButton.setBackground(MaterialColors.ORANGE_800);
        clearButton.setForeground(MaterialColors.WHITE);
        clearButton.addMouseListener(MaterialUIMovement.getMovement(clearButton, MaterialColors.ORANGE_500));

        submitButton.setBackground(MaterialColors.ORANGE_800);
        submitButton.setForeground(MaterialColors.WHITE);
        submitButton.addMouseListener(MaterialUIMovement.getMovement(submitButton, MaterialColors.ORANGE_500));

        individualButton.setBackground(MaterialColors.ORANGE_800);
        individualButton.setForeground(MaterialColors.WHITE);
        individualButton.addMouseListener(MaterialUIMovement.getMovement(individualButton, MaterialColors.ORANGE_500));
        //endregion
        //region scroll pane
        arrayTab.getVerticalScrollBar().setUnitIncrement(25);
        arrayTab.getHorizontalScrollBar().setUnitIncrement(25);

        stepsTab.getVerticalScrollBar().setUnitIncrement(25);
        stepsTab.getHorizontalScrollBar().setUnitIncrement(25);

        enterByHandTab.getVerticalScrollBar().setUnitIncrement(25);
        enterByHandTab.getHorizontalScrollBar().setUnitIncrement(25);
        //endregion

        setDefaultTextFields();
        setDefaultLabels();

        frame.add(panel);
    }

    @Override
    void actionListener() {
        exitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                frame.dispatchEvent(new WindowEvent(frame, WindowEvent.WINDOW_CLOSING));
            }
        });
        randomButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                if (tryParse("Random")) {
                    search = new Search(min, max, length);
                    if(tryParse(searchElementTextField)){
                        search.search(Integer.parseInt(searchElementTextField.getText()));
                    }
                    setLabels();
                } else massageError();
            }
        });
        byHandButton.addActionListener(new ActionListener() {
            boolean isInitialized = false;

            @Override
            public void actionPerformed(ActionEvent e) {
                if (tryParse("ByHand")) {
                    if (!isInitialized) {
                        isInitialized = true;
                        enterLengthLabel.setVisible(false);
                        createTable();
                    } else massageError("Table is initialized already");
                } else massageError();
            }
        });
        submitButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                try {
                    getTableContent();
                    if(tryParse(searchElementTextField)){
                        search.search(Integer.parseInt(searchElementTextField.getText()));
                    }
                    setLabels();
                } catch (Exception ex) {
                    massageError();
                }
            }
        });
        clearButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                setDefaultLabels();
                setDefaultTextFields();
                showAdditionalInfo("Clear");
            }
        });
//        saveButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                if (sort.getArray() == null) {
//                    massageError("Please, initialize matrix firstly");
//                } else {
//                    save(sort.getArray());
//                    showAdditionalInfo("Save");
//                }
//            }
//        });
//        loadButton.addActionListener(new ActionListener() {
//            @Override
//            public void actionPerformed(ActionEvent e) {
//                load();
//                showAdditionalInfo("Load");
//                sort.sort();
//                setLabels();
//            }
//        });
        individualButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                massagePlain("<html>Складіть програму,<br/>" +
                        "яка в невпорядкованій таблиці з 20 цілих чисел<br/>" +
                        "із діапазону [–10; 10], визначених датчиком<br/>" +
                        "випадкових чисел, здійснює пошук числа 0. Якщо таких<br/>" +
                        "декілька, знайти перше. Вивести його позицію.</html>", "Варіант 8");
            }
        });
    }

    @Override
    void mouseListener() {
        lengthTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON3) {
                    lengthTextField.setText(null);
                }
            }
        });
        minTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON3) {
                    minTextField.setText(null);
                }
            }
        });
        maxTextField.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2 && e.getButton() == MouseEvent.BUTTON3) {
                    maxTextField.setText(null);
                }
            }
        });
    }

    private boolean tryParse(String type) {
        if (type == "Random") {
            if (tryParse(lengthTextField)
                    && tryParse(minTextField) && tryParse(maxTextField)) {
                length = Integer.parseInt(lengthTextField.getText());
                min = Integer.parseInt(minTextField.getText());
                max = Integer.parseInt(maxTextField.getText());
                return true;
            }
        } else if (type == "ByHand") {
            if (tryParse(lengthTextField)) {
                length = Integer.parseInt(lengthTextField.getText());
                return true;
            }
        }
        return false;
    }

    private void createUIComponents() {
        // TODO: place custom component creation code here

    }

    private void createTable() {
        table = new JTable(1, length);

        System.out.println(table.getRowCount());
        System.out.println(table.getColumnCount());
        clearTable();
        enterByHandPanel.add(table);
    }

    private void clearTable() {
        try{
            for (int i = 0; i < length; i++) {
                table.setValueAt(0, 0, i);
            }
        }catch (ArrayIndexOutOfBoundsException ex){
            System.out.println("table isn`t initializated");
        }
    }

    private void getTableContent() {
        ArrayList<Integer> array = new ArrayList<>();
        for (int i = 0; i < length; i++) {
            String s = String.valueOf(table.getValueAt(0, i));
            array.add(Integer.parseInt(s));
        }
        search.setArray(toArray(array));
    }

    private void setDefaultTextFields() {
        lengthTextField.setText(String.valueOf(DEFAULT_LENGTH));
        minTextField.setText(String.valueOf(DEFAULT_MIN));
        maxTextField.setText(String.valueOf(DEFAULT_MAX));
    }

    private void setDefaultLabels() {
        clearedLabel.setVisible(false);

        arrayLabel.setText("Here is must be unsorted array!");

        stepsLabel.setText("Here is must be steps!");
        enterLengthLabel.setText("Enter firstly length!");
        timeOfAlgorithmLabel.setText(null);
        countOfComparationLabel.setText(null);
        indexOfSearchingElementLabel.setText(null);

        clearTable();
    }

    private void setLabels() {
        arrayLabel.setText(Arrays.toString(search.getArray()));
        timeOfAlgorithmLabel.setText(search.getTime() + "ns.");
        countOfComparationLabel.setText(search.getCountOfComparing() + "");
        indexOfSearchingElementLabel.setText(search.getSearchIndex() + "");

        stepsLabel.setText(search.getSteps());
    }

    private void showAdditionalInfo(String type) {

        if (type == "Save") {
            clearedLabel.setVisible(false);
        } else if (type == "Load") {
            clearedLabel.setVisible(false);
        } else if (type == "Clear") {
            clearedLabel.setVisible(true);
        }

        Timer timer = new Timer();
        timer.schedule(new TimerTask() {
            @Override
            public void run() {
                clearedLabel.setVisible(false);
            }
        }, 5 * 1000);

    }
}
