package com.aftermoonest.gui;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

public abstract class MainFrame extends JFrame {
    static final int SIZE_OF_FRAME_X = 700;
    static final int SIZE_OF_FRAME_Y = 800;

    JFrame frame = new JFrame();

    public void run() {
        config();
        createView();
        actionListener();
        mouseListener();
    }

    private void config() {
        frame.setLayout(new GridBagLayout());
        frame.setVisible(true);
        frame.setResizable(false);
        frame.setSize(SIZE_OF_FRAME_X, SIZE_OF_FRAME_Y);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
    }

    boolean tryParse(JTextField textField) {
        String text = textField.getText();

        try {
            int number = Integer.parseInt(text);
            return true;
        } catch (NumberFormatException ex) {
            textField.setText(null);
            return false;
        }
    }

    void massagePlain(String textMassage, String title) {
        JOptionPane optionPane = new JOptionPane();
        JOptionPane.showMessageDialog(frame, textMassage, title, JOptionPane.PLAIN_MESSAGE);
    }

    void massageError(String textError) {
        JOptionPane optionPane = new JOptionPane();
        JOptionPane.showMessageDialog(frame, textError, "Error", JOptionPane.ERROR_MESSAGE);
    }

    void massageError() {
        JOptionPane optionPane = new JOptionPane();
        JOptionPane.showMessageDialog(frame, "Ooops. Something wrong", "Error", JOptionPane.ERROR_MESSAGE);
    }

    int[] toArray(ArrayList<Integer> arrayList){
        int[] array = new int[arrayList.size()];
        for (int i = 0; i < arrayList.size(); i++) {
            array[i] = arrayList.get(i);
        }
        return array;
    }

    abstract void createView();

    abstract void actionListener();

    abstract void mouseListener();
}
