package com.aftermoonest.search;

public class Search {
    private int[] array;

    public int[] getArray() {
        return array;
    }

    public void setArray(int[] array) {
        this.array = array;
    }

    public int getSearchIndex() {
        return searchIndex;
    }

    private int searchIndex;
    private int searchElement;

    public int getSearchElement() {
        return searchElement;
    }

    private double time;

    public double getTime() {
        return time;
    }

    public String getSteps() {
        return steps;
    }

    public int getCountOfComparing() {
        return countOfComparing;
    }

    private int countOfComparing = 0;

    private String steps = "<html>";

    public Search() {
        array = Random.random();
    }

    public Search(int minElement, int maxElement) {
        array = Random.random(minElement, maxElement);
    }

    public Search(int minElement, int maxElement, int length) {
        array = Random.random(minElement, maxElement, length);
    }

    public void search(int searchElement) {
        double timeStart = System.nanoTime();
        this.searchElement = searchElement;
        linearSearch();
        time = System.nanoTime() - timeStart;
    }

    private int linearSearch() {
        steps += "Start searching element - " + searchElement + "<br/>";
        for (int i = 0; i < array.length; i++) {
            steps += "Check index [" + i + "]<br/>";
            steps += "Index [" + i + "] = " + array[i] + "<br/>";
            countOfComparing++;
            if (array[i] == searchElement) {
                steps += "Yey, " + array[i] + " == " + "searching element" + "<br/>";
                steps += "Index of element is: [" + i + "]<br/>";
                searchIndex = i;

                return searchIndex;
            } else {
                steps += "Element " + array[i] + " != " + searchElement + "<br/>";
                steps += "Keep searching" + "<br/><br/>";
            }
        }
        steps += "There is no " + searchElement + " in array. We`re sorry you. Try again and good luck" + "<br/>";
        steps += "</html>";
        searchIndex = -1;
        return -1;
    }
}
