package com.aftermoonest.search;

public class Random {

    private static final int LENGTH = 20;
    private static final int MIN = -10;
    private static final int MAX = 10;

    public static int[] random() {
        return randomMethod(MIN, MAX, LENGTH);
    }

    public static int[] random(int min, int max) {
        return randomMethod(min, max, LENGTH);
    }

    public static int[] random(int min, int max, int length){
        return randomMethod(min, max, length);
    }

    private static int[] randomMethod(int min, int max, int length){
        int[] array = new int[length];
        for (int i = 0; i < array.length; i++) {
            array[i] = (int) (Math.random() * (max - min) + min);
        }
        return array;
    }
}
