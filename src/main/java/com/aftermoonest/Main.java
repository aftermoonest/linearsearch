package com.aftermoonest;

import com.aftermoonest.gui.Frame;
import com.aftermoonest.search.Random;
import com.aftermoonest.search.Search;
import mdlaf.MaterialLookAndFeel;

import javax.swing.*;

public class Main {
    public static void main(String[] args) {
//        Search search = new Search();
//        search.search(0);
        try {
            JDialog.setDefaultLookAndFeelDecorated(true);
            UIManager.setLookAndFeel(new MaterialLookAndFeel());
        } catch (UnsupportedLookAndFeelException var36) {
            var36.printStackTrace();
        }
        Frame frame = new Frame();
        frame.run();
    }
}
